FROM openjdk

COPY target .
ENV Param1=-Dspring.profiles.active=h2
ENV Param2=-jar 

RUN useradd Marianna
USER Marianna 

CMD java $Param2 $Param1 *.jar
EXPOSE 8090
